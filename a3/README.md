> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Assignment 3 Requirements:

*Four Parts:*

1. Develop app about that tells user how much money will they spend on concert tickets
2. Created and forward engineered database for pets, pet stores, and customers
3. Made and implemented launcher icon for the devloped mobile application


### Business rules and requirements for database

> 1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer. 
> 2. A store has many pets, but each pet is sold by only one store.

> Requirements for database

* Can a customer exist without a pet? Seems reasonable. Yes. (optional) 
* Can a pet exist without a customer? Again, yes. (optional) 
* Can a pet store not have any pets? It wouldn’t be a pet store. (mandatory) 
* Can a pet exist without a pet store? Not in this design. (mandatory)


#### Assignment Screenshots:

*Screenshot of database ERD*:

![a3 ERD screenshot](img/erd.png "screenshot of ERD")

*Screenshot of database data*:

|             |     Databases Data
| ----------- | ------------------------------ |
|    Pet table     | ![Pet table](img/pet.png) |
|   Petstore table  | ![Petstore table](img/petstore.png) |
|   Customer table  | ![customer table](img/customer.png) |

*Screenshot of My Even app running*:


|             |     App Running
| ----------- | ------------------------------ |
|    Main     | ![Main page](img/app_main.jpg) |
|   Processs  | ![Process](img/app_working.jpg) |
|   Prompt  | ![Prompt page](img/app_prompt.jpg) |


*Screenshots of skill set programs*: 

|             |     Skill sets 4 - 6
| ----------- | ------------------------------ |
|    Skill set 4     | ![SS4_screenshot](img/ss4.png) |
|    SKill set 5  | ![SS5_screenshot](img/ss5.png) |
|    Skill set 6  | ![SS6_screenshot](img/ss6.png) |



*Links to database resources*

|        ERD     |     SQL script
| -------------- | ------------------------------ |
|    [ERD](a3.mwb)     | [SQL_script](a3.sql) |
