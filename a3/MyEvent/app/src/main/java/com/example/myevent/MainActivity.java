package com.example.myevent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    //initializes variables
    int numberOfTickets = 0;
    double costPerTicket = 0.0;
    double totalCost = 0.0;
    String groupChoice = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //begin code here
        //show app icon in emulator
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText tickets = (EditText)findViewById(R.id.txtTicket);
        final Spinner group = (Spinner)findViewById(R.id.txtGroup);
        Button cost = (Button)findViewById(R.id.txtBtnCost);
        cost.setOnClickListener(new View.OnClickListener(){
            final TextView result = ((TextView)findViewById(R.id.txtResult));
            @Override
            public void onClick(View v)
            {
                //returns string representation of selected item object from spinner control
                groupChoice = group.getSelectedItem().toString();
                if(groupChoice.equals("Lucas von Hollen"))
                {
                    costPerTicket = 59.99;
                }
                else if(groupChoice.equals("Mark Jowett"))
                {
                    costPerTicket = 99.99;
                }
                else
                {
                    costPerTicket = 69.99;
                }
                //returns string representation from EditText control and converts to int
                numberOfTickets = Integer.parseInt(tickets.getText().toString());
                totalCost = costPerTicket * numberOfTickets;
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
                result.setText("Cost for " + groupChoice + " is " + nf.format(totalCost));
            }
        });

    }
}