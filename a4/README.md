> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Advanced Mobile Web Application

## Yanheng Chen

### Assignment 4 Requirements:

*Three Parts:*

1. Developed webpage with Bootstrap framework
2. Used PHP to do client-side data validation
3. Skillsets 10 to 12


#### Assignment Screenshots:

*Screenshot of PHP Client Side Validation*

|             |     Validation
| ----------- | ------------------------------ |
|    Not valid     | ![Pet table](img/phpvalidation1.png) |
|   Valid  | ![Petstore table](img/phpvalidation2.png) |


*Screenshot of front page*
![Petstore table](img/carosel.png)

*Screenshot of Skill sets*

|             |     Skill sets
| ----------- | ------------------------------ |
|    Skill set 10    | ![Pet table](img/ss10.png) |
|   Skill set 11  | ![Petstore table](img/ss11.png) |
|   Skill set 12  | ![Petstore table](img/ss12.png) |




