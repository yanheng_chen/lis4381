> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App 

## Yanheng Chen

### Project 1 Requirements:

*Three Parts:*

1. Developed app that shows my basic information (my business card)
2. Decorated my business card with text-shadows and image borders
3. Developed skill sets 7, 8, and 9




#### Assignment Screenshots:

*Screenshot of My Business Card App*:

|      Main Page       |     Details Page
| ----------- | ------------------------------ |
|    ![Pet table](img/main_page.png)     | ![Pet table](img/details_page.png) |

*Screenshot of Skill set 7*:

![skill set 7](img/ss7.png)

*Screenshot of Skill set 8*:

![skill set 8](img/ss8.png)

*SScreenshot of Skill set 9*:

![skill set 9](img/ss9.png)


