package com.example.mybusinesscard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybusinesscard.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //setContentView() indicates necessary application resources location
        //"R" represents resource directory
        //"Layout" refers to "layout" directory, accessing activity_main.xml file
        //begin code here
        //show app icon in emulator
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        Button button = (Button) findViewById(R.id.btnRecipe);
        //instantiate button object variable "button" that refers to btnRecipe

        //associate OnClickListener with the button object variable
        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Details.class));
            }



        });

    }
}