<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Yanheng Chen">
		<link rel="icon" href="favicon.ico">

		<title>Lis 4381 Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h2
		 {
			 margin: 0;     
			 color: #1BACFF;
			 padding-top: 50px;
			 font-size: 60px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 .item
		 {
			 background: #333;    
			 text-align: center;
			 height: 600px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 } 
		 .bs-example
		 {
			 margin: 20px;
		 }
		 .navbar-custom 
		 {
    		background-color: #4433cc;
		}
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="3000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/Helpdesk-8_c.jpeg) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>Yanheng Chen</h3>
										<p class="lead">I am a student studying IT in FSU</p>
										<a class="btn btn-primary" href="https://bitbucket.org/yanheng_chen/lis4381/src/master/">Learn More about me</a>
                        </div>
                      </div>
                    </div>

              
						<div class="item">
							<a href="https://bitbucket.org/yanheng_chen/lis4381/src/master/"> 
								<img src="img/app.jpg" alt="Slide 2">
							</a>	
								<h2>Slide 2</h2>
								<div class="carousel-caption">
									<h3>Android products</h3>
									<p>These are some of the projects that I have been working on for my LIS 4381 course</p>								
								</div>
							</div>

							<div class="item">
		 						<a href="https://helpdesk.cci.fsu.edu/about/">
									<img src="img/fsu.png" alt="Slide 3">
								</a>
								<h2>Slide 3</h2>
								<div class="carousel-caption">
									<h3>Additional experience</h3>
									<p>This will take you to the website about my Help Desk job at my college</p>									
								</div>
							</div>

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
