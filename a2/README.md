> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Applications

## Yanheng Chen

### Assignment 2 Requirements:

*Three Parts:*

1. Made mobile app using Android Studios
2. Developed programs using things Java according to the requirements of skill sets problems
3. Ran mobile app using emulator




#### Assignment Screenshots:

*Screenshot of main screen of finished mobile app*:

![healthy_recipe_launch_screen](img/heathy_recipe_main.png "healthy recipe launch screen")

*Screenshot of recipe page of finished mobile app*:

![healthy_recipe_recipe_page](img/heathy_recipe_recipe.png)

*Screenshot of skill set 1 and its requirements*:

![Even or odd](img/ss1.png)

*Screenshot of skill set 2 and its requirements*:

![Largest Number](img/ss2.png)

*Screenshot of skill set 3 and its requirements*:

![Loops and arrays](img/ss3.png)




