<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Develop app to show Bruchetta recipe and worked on assignmnet skill sets ">
		<meta name="author" content="Yanheng Chen">
    <link rel="icon" href="../favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Develop app to show Bruchetta recipe and worked on assignmnet skill sets 
				</p>

				<h4>Health Recipes app running</h4>
				<img src="img/heathy_recipe_main.png" class="img-responsive center-block" alt="Healthy Recipes Main">
				<img src="img/heathy_recipe_recipe.png" class="img-responsive center-block" alt="Healthy Recipes recipes page">

				<h4>Skill sets program</h4>
				<img src="img/ss1.png" class="img-responsive center-block" alt="Android Studio Installation">
				<img src="img/ss2.png" class="img-responsive center-block" alt="Android Studio Installation">
				<img src="img/ss3.png" class="img-responsive center-block" alt="Android Studio Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
