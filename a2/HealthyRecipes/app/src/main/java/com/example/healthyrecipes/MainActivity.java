package com.example.healthyrecipes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //setContentView() indicates necessary application resources location
        //"R" represents resource directory
        //"Layout" refers to "layout" directory, accessing activity_main.xml file

        Button button = (Button) findViewById(R.id.btnRecipe);
        //instantiate button object variable "button" that refers to btnRecipe

        //associate OnClickListener with the button object variable
        button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Recipe.class));
            }



        });

    }
}