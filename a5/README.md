> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Yanheng Chen

### Assignment 5 Requirements:

*Four Parts:*

1. PHP server side validation
2. Connect database to web application
3. Skill sets 13, 14, and 15
4. Show database values in data tables



#### Assignment Screenshots:

*Screenshot of PHP Server Side Validation*

|             |     Validation and Data Table
| ----------- | ------------------------------ |
|      Data Table   | ![Data table](img/a5.png) |
|      Data Table 2   | ![Data table](img/a5_2.png)
|   InValid  | ![Invalid](img/error.png) |

*Screenshot of Skill Set 13*
![Data table](img/ss_13.png)

*Screenshot of Skill Set 14*

|             |     Skill Set 14
| ----------- | ------------------------------ |
|      Calculator   | ![Data table](img/ss14.png) |
|      Addition   | ![Data table](img/ss14_a.png)
|   Subtraction | ![Invalid](img/ss14_s.png) |
|      Multiplication   | ![Data table](img/ss14_m.png) |
|      Division   | ![Data table](img/ss14_d.png)
|  Exponents  | ![Invalid](img/ss14_e.png) |

*Screenshot of Skill Set 15*

|             |     Skill Set 15
| ----------- | ------------------------------ |
|      Enter User Text   | ![Data table](img/ss15_1.png) |
|      Show User Text   | ![Data table](img/ss15_2.png)|