> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**


# Course LIS4381 Mobile Web Application Development

## Yanheng Chen


### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Course Installations: Andriod Studios, Java, and AMPSS
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Worked on skill sets 1, 2 and 3
    - Coded app using Andriod Studios
    - Imported images and texts into app created
    - Ran app using emulator and Samsung phone

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Worked on skill sets 4, 5, and 6
    - Developed app that determines how much one would spent in one event
    - Developed database under specific business rules

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Worked on skill sets 10, 11, and 12
    - Developed web application using PHP
    - Developed client side validation of forms using Bootstrap

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Worked on skill sets 13, 14, and 15
    - Developed web application using PHP
    - Developed server side validation of forms using Bootstrap

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Developed app that will show basic information about myself
    - Created image border and text shadows as decorations
    - Worked on skill sets 7, 8, and 9

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Finished creating the Add and Delete button for the data on index.php
    - Allow user to edit and delete their record
    - Created a RSS feed