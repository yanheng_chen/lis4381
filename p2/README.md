> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 -- Mobile Web Applications 
## Yanheng Chen

### Project 2 Requirements:

*Three Parts:*

1. Created edit button for user to edit a record
2. Created delete button for user to delete a record
3. Created RSS feed



#### Assignment Screenshots:

*RSS feed*
![Data table](img/rss.png) |

*Screenshot of Edit, delete, and add records*

|             |     Before                     |  After                   |  
| ----------- | ------------------------------ |--------------------------|
|  Edit Record |   ![Data table](img/edit1.png)  | ![Data table](img/edit2.png)
|      Delete Record   | ![Data table](img/del.png) | ![Data table](img/deld.png)
|   Add Record | ![Invalid](img/add.png) | ![Data table](img/edit.png)



