<?php
/*
	 //show  errors
   ini_set('display_errors', 1)
   
*/

error_reporting(E_All);
//get item ID
$pst_id_v = $_POST['pst_id'];

//pull in database connection
require_once "../global/connection.php";

//find out which PDO drivers are available
//print_r(PDO::getAvailableDrivers());

/*
Why prepared statements?
1) Prevent SQL injection.
2) Process faster.

Resource (and many others): http://www.php.net/pdo.prepared-statements
*/

//query to delete selected item from database
$query = 
"Delete from petstore
where pst_id = :pst_id_p
";


//display query statement, then exit (for testing purposes only)
//exit($query);
try{
// 2) prepare(): Prepares a statement for execution and returns a statement object
//The SQL statement can contain zero or more named (:name) or question mark (?) parameter markers 
//for which real values will be substituted when the statement is executed.
$statement = $db->prepare($query);

// 3) bindParam(): Binds a value to a parameter
$statement->bindParam(':pst_id_p', $pst_id_v);

// 4) Executes a prepared statement
$row_count = $statement->execute();
$statement->closeCursor();

header('Location: index.php'); //redirects back to index.php
}
/*
NOTE: In general, prepared statements only protect from SQL injection when using bindParam or bindValue option.
However, it is possible to pass an array of values that would also be secured against SQL injection. 
In this case you don't necessarily have to use bindParam() or bindValue().
http://www.php.net/manual/en/pdo.prepare.php
*/

catch (PDOException $e)
{
  $error = $e->getMessage();
  include('../global/error.php');
}
?>
