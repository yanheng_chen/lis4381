import java.util.*;
import java.util.Scanner; //import the scanner documentation

import javax.print.DocPrintJob;

import java.lang.*;//import the math documentation
import java.text.*;

public class Methods {
    // create global scanner object sc so it can be used by multiple methods of the
    // Methods class
    // "final" prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen\n");

        System.out.println("Temperature Conversion Program \n");

        System.out.println("Program converts user-entered temperatures into Fahrenheit or Celsius scales.");

        System.out.println("Program continues to prompt for user entry until no longer requested");

        System.out.println("Note: lower or upper case letter permitted. Though, incorrect entries are not permitted.");

        System.out.println("Note: Program does nto validate numeric data \n");

    }

    public static void converTemp() {
        double temperature = 0.0;
        char choice = ' '; //initialize to space character
        char type = ' '; //initialize to space character

        do 
        {
            System.out.print("Fahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\": ");
            type = sc.next().charAt(0);
            type = Character.toLowerCase(type);

            if(type == 'f')
            {
                System.out.print("Enter temperature in Fahrenheit: ");
                temperature = sc.nextDouble();
                temperature = (temperature - 32 * 5)/9;
                System.out.println("Temperature in Celsius = " + temperature);

            }
            else if (type == 'c')
            {
                System.out.print("Enter temperature in Celsius: ");
                temperature = sc.nextDouble();
                temperature = (temperature * 9 / 5 ) + 32;
                System.out.println("Temperature in Fahrenheit = " + temperature);
            }

            else 
            {
                System.out.println("Incorrect entry. Please try again.");
            }
            System.out.println("\nDo you want to convert a temperature(y or n)");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while (choice == 'y');
    }

}
// end of the class
