public class Main {
    public static void main(String[] args) {
        Methods.getRequirements();

        // initialize arraySize variable
        int arraySize = 0;

        // populate arraySize variable after data validation
        arraySize = Methods.validateArrarySize();

        Methods.calculateNumbers(arraySize);
    }
}
