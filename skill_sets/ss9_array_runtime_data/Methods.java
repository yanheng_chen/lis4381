import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation
import java.text.*;

public class Methods {
    // create global scanner object sc so it can be used by multiple methods of the
    // Methods class
    // "final" prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen\n");

        System.out.println("1) Program creates array size at run-time");

        System.out.println("2) Program displays array size");

        System.out.println("3) Program rounds sum and average of numbers to two decimal places");

        System.out.println("4) Numbers *must* be float data type, not double\n");

    }

    public static int validateArrarySize() {
        // declare arraySize variable
        int arraySize = 0;

        System.out.print("Please enter array size: ");

        while (!sc.hasNextInt()) {
            System.out.println("Not a valid integer!\n");
            sc.next(); // If this is omitted, it will go into an infinite loop as the user can never
                       // input anything
            System.out.print("Please try again. Enter array size: ");
        }
        arraySize = sc.nextInt(); // valid int stored into arraySize

        System.out.println(); // prints blank line

        return arraySize;
    }

    public static void calculateNumbers(int arraySize) {
        // initalizes both sum and average as float variables
        float sum = 0.0f;
        float average = 0.0F;

        // indicate number of values required, based upon user input
        System.out.print("Please enter " + arraySize + " numbers:  \n");

        // create array for storing user input, based upon user-entered array size
        float numsArray[] = new float[arraySize];

        // validate data entry
        for (int i = 0; i < arraySize; i++) {
            System.out.print("Enter num: " + (i + 1) + " ");

            while (!sc.hasNextFloat()) {
                System.out.println("Not valid number \n");
                sc.next(); // If this is omitted, it will go into an infinite loop as the user can never
                           // input anything
                System.out.print("Please try again. Enter num " + (i + 1) + ": ");
            }
            numsArray[i] = sc.nextFloat(); // populates numsArray with valid float value from user
            sum = sum + numsArray[i];
        }
        average = sum / arraySize; // gets average of all of the user entered numbers in the array

        // print numbers entered
        System.out.print("\nNumbers entered: ");
        for (int u = 0; u < arraySize; u++) {
            System.out.println(numsArray[u] + " ");
        }

        // call method to print and format numbers
        printNumbers(sum, average);
    }

    public static void printNumbers(float sum, float average) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        System.out.println("Sum: " + df.format(sum));
        System.out.println("Average: " + df.format(average));
    }

}
// end of the class
