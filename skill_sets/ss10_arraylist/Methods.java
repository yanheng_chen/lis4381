import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation
import java.text.*;

public class Methods {
    // create global scanner object sc so it can be used by multiple methods of the
    // Methods class
    // "final" prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen\n");

        System.out.println("Program populates ArrayList of Strings with user-entered animal type values.");

        System.out.println("Examples: Polar bear, Guinea pig, dog, car, bird.");

        System.out.println("Program continues to collect user-entered values until user types \"n\"");

        System.out.println("Program displays ArrayList values after each iteration, as well as size. \n");

    }

    public static void createArrayList() {
        // create scanner object
        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>(); // create String type ArrayList
        String myStr = ""; // Create String object myStr
        String choice = "y"; // Initalize String object choice by assigning it to "y"
        int num = 0; // Initalize int variable num to 0

        // populate ArrayList using while loop
        while (choice.equals("y")) {
            System.out.print("Enter animal type: ");
            myStr = sc.nextLine();
            obj.add(myStr); // add String object
            num = obj.size(); // return ArrayList Size
            System.out.println("ArrayList elements: " + obj + "\nArrayList Size = " + num);
            System.out.print("\nContinue? Enter y or n: ");
            choice = sc.next().toLowerCase(); // lets user enter capital or lower case y or n
            sc.nextLine();
        }
    }

}
// end of the class
