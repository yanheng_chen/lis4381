import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation
import java.text.*;

public class Methods {
    // create global scanner object sc so it can be used by multiple methods of the
    // Methods class
    // "final" prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen\n");

        System.out.println("1) Program determines whether user entered value is alpha, numeric, or special characters");

    }

    public static void determineChar() {
        // create scanner object
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter character: ");

        // next() function returns next token
        // Token: smallest element of a program meaningful to complier/interpreter
        // takes the first character of the token and stores it into char variable ch
        char ch = sc.next().charAt(0);

        // tests for alpha characters
        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
            System.out.println(ch + "is alpha. ASCII value: " + (int) ch); // casts char to int

        }
        // tests for numeric characters
        else if (ch >= '0' && ch <= '9') {
            System.out.println(ch + " is numeric. ASCII value: " + (int) ch);

        }
        // otherwise, it must be a special character
        else {
            System.out.println(ch + " is special character. ASCII value: " + (int) ch);
        }

    }

}
// end of the class
