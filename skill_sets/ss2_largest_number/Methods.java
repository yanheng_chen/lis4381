import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen");

        System.out.println("This program takes two numbers and compare which one is bigger");
    }

    public static void largestNumber() {
        // initialize int variable num1, num2
        int num1, num2;
        // create new scanner object scnr
        Scanner scnr = new Scanner(System.in);

        System.out.print("Enter number 1: ");
        num1 = scnr.nextInt();

        System.out.print("Enter number 2: ");
        num2 = scnr.nextInt();

        if (num2 < num1) {
            System.out.print(num1 + " is the bigger number than " + num2);
        }

        if (num1 < num2) {
            System.out.print(num2 + " is the bigger than " + num1);
        }

        if (num1 == num2) {
            System.out.print("They are all equal ");
        }
    }

}
// end of the class
