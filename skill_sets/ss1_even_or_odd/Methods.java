import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen");

        System.out.println("This program takes a number in and determines whether or not it is even or odd number");
    }

    public static void evenodd() {
        // initialize int variable num1, num2
        int num1, num2;
        // create new scanner object scne
        Scanner scnr = new Scanner(System.in);

        System.out.print("Enter number: ");
        num1 = scnr.nextInt();

        num2 = num1 % 2;

        if (num2 == 0) {
            System.out.print("It is even");
        } else {
            System.out.print("It is odd");
        }
    }

}
// end of the class
