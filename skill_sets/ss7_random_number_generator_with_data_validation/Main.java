
public class Main {
    public static void main(String[] args) {
        Methods.getRequirements();

        //returns initalized array, array size determined by user
        int[] userArray = Methods.createArray();


        Methods.generatePseudoRandomNumbers(userArray);
    }
}