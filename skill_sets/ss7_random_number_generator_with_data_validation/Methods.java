import java.util.*;
import java.util.Scanner; //import the scanner documentation
import java.lang.*;//import the math documentation
import java.util.Random;

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen");

        System.out.println("Print minimum and maximum integer values: ");

        System.out.println("Program prompts user to enter desired numbers of pseudorandom-generated integers (min 1).");

        System.out.println("Program validates user input for integers greater than 0. ");

        System.out.println("Use following loop structures: for, enhanced for, while, do...while. \n");
    }

    public static int[] createArray() {

        // create new scanner object scne
        Scanner scnr = new Scanner(System.in);

        System.out.print("Enter desired number of pseudo-random integers (min 1): ");

        // initialize int variable arraySize
        int arraySize = 0; // used to iterate loops

        while (!scnr.hasNextInt()) {
            System.out.println("Not valid integer");

            scnr.next(); // makes user input another value

            System.out.println("Please try again. Enter valid integer(min 1): ");
        }

        arraySize = scnr.nextInt(); // valid int but not greater than 0

        while (arraySize < 1) {
            // include data validation
            System.out.println("\nNumber must be greater than 0. Please enter integer greater than 0: ");
            while (!scnr.hasNextInt()) {
                System.out.print("\nNumber must be integer: ");
                scnr.next();
                System.out.print("Please try again. Enter integer value greater than 0: ");
            }
            arraySize = scnr.nextInt(); // valid int greater than 0
        }

        int yourArray[] = new int[arraySize];
        return yourArray;
    }

    public static void generatePseudoRandomNumbers(int[] myArray) {
        Random r = new Random(); // instantiate random object variable
        int i = 0;

        System.out.println("for loop: ");

        for (i = 0; i < myArray.length; i++) {
            System.out.println(r.nextInt(10) + 1); // print pseudorandom number between 1 and 10, inclusive
        }

        System.out.println("\nEnhanced for loop");
        for (int n : myArray) {
            System.out.println(r.nextInt());
        }

        System.out.println("\nwhile loop");
        i = 0; // reassining i to 0
        while (i < myArray.length) {
            System.out.println(r.nextInt());
            i++;
        }

        i = 0; // reassining i to zero
        System.out.println("\nDo...while loop");
        do {
            System.out.println(r.nextInt());
            i++;
        } while (i < myArray.length);
    }

    // value-returning method (static requires no object)
    /*
     * public static void validateUserInput() {
     * 
     * // declare variables and create Scanner object
     * Scanner scnr = new Scanner(System.in);
     * int num1 = 0, num2 = 0, num3 = 0;
     * 
     * // prompt user for three integers
     * System.out.println("Please enter first number. ");
     * 
     * while (!scnr.hasNextInt()) {
     * System.out.println("Not a valid integer. \n");
     * scnr.next(); // if omitted, the user will never get their chance to input
     * their value
     * System.out.print("Please try again. Enter first number. ");
     * }
     * num1 = scnr.nextInt();
     * 
     * while (!scnr.hasNextInt()) {
     * System.out.println("Not a valid integer. \n");
     * scnr.next(); // if omitted, the user will never get their chance to input
     * their value
     * System.out.print("Please try again. Enter second number. ");
     * }
     * num2 = scnr.nextInt();
     * 
     * while (!scnr.hasNextInt()) {
     * System.out.println("Not a valid integer. \n");
     * scnr.next(); // if omitted, the user will never get their chance to input
     * their value
     * System.out.print("Please try again. Enter third number. ");
     * }
     * num3 = scnr.nextInt();
     * 
     * System.out.println();
     * 
     * getLargestNumber(num1, num2, num3);
     * 
     * }
     */

}
// end of the class
