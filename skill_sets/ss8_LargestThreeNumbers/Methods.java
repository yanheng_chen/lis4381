import java.util.*;
import java.util.Scanner; //import the scanner documentation

import javax.lang.model.util.ElementScanner14;

import java.lang.*;//import the math documentation

public class Methods {

    public static void getRequirements() {

        System.out.println("Made by Yanheng Chen");

        System.out.println("This program takes three numbers in and evaulate which is the largest.\n");

        System.out.println("Note: this program checks for integers and non-numeric values.\n");
    }

    // value-returning method (static requires no object)
    public static void validateUserInput() {

        // declare variables and create Scanner object
        Scanner scnr = new Scanner(System.in);
        int num1 = 0, num2 = 0, num3 = 0;

        // prompt user for three integers
        System.out.println("Please enter first number: ");

        while (!scnr.hasNextInt()) {
            System.out.println("Not a valid integer. \n");
            scnr.next(); // if omitted, the user will never get their chance to input their value
            System.out.print("Please try again. Enter first number: ");
        }
        num1 = scnr.nextInt();

            System.out.println("\nPlease enter second number: ");
        while (!scnr.hasNextInt()) {
            System.out.println("Not a valid integer. \n");
            scnr.next(); // if omitted, the user will never get their chance to input their value
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = scnr.nextInt();

        System.out.println("\nPlease enter third number: ");
        while (!scnr.hasNextInt()) {
            System.out.println("Not a valid integer. \n");
            scnr.next(); // if omitted, the user will never get their chance to input their value
            System.out.print("Please try again. Enter third number: ");
        }
        num3 = scnr.nextInt();

        System.out.println();

        getLargestNumber(num1, num2, num3);

    }

    public static void getLargestNumber(int num1, int num2, int num3) {
        System.out.println("Numbers Entered: " + num1 + ", " + num2 + ", " + num3);

        if (num1 > num2 && num1 > num3)
            System.out.println(num1 + " is largest.");
        else if (num2 > num1 && num2 > num3)
            System.out.println(num2 + " is largest.");
        else if (num3 > num2 && num3 > num1)
            System.out.println(num3 + " is largest.");
        else
            System.out.println("Integers are equal");
    }
}
// end of the class
