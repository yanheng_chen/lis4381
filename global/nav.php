
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" target="_self">Home</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    					<span class="navbar-toggler-icon"></span>
  				</button>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				<a class="navbar-brand" href="#">
				</a>
					<li class="active"><a href="../index.php">Lis 4381 Advanced Mobile Web Application</a></li>
					<li class="nav-item"><a href="../a1/index.php">A1</a></li>
					<li class="nav-item"><a href="../a2/index.php">A2</a></li>
					<li class="nav-item"><a href="../a3/index.php">A3</a></li>
					<li class="nav-item"><a href="../a4/index.php">A4</a></li>
					<li class="nav-item"><a href="../a5/index.php">A5</a></li>
					<li class="nav-item"><a href="../p1/index.php">P1</a></li>
					<li class="nav-item"><a href="../p2/index.php">P2</a></li>
					<li class="nav-item dropdown">
        				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          					Skill Sets
        				</a>
        				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          					<a class="dropdown-item" href="../skill_sets/ss14_simple_calculator_PHP">Skill Set 14</a>
          					<a class="dropdown-item" href="../skill_sets/ss15_read_write_files_PHP">Skill Set 15</a>
        				</div>
					</li>					
				</ul>
				
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<?php
date_default_timezone_set('America/New_York');
$today = date("m/d/y g:ia");
echo $today;
 ?>
	
