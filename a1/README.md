> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Yanheng Chen

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Android Studio and Java first app


> #### Git commands w/short descriptions:

1. git init - initialize the repo locally
2. git status - describes the state of the current working directory and the staging area
3. git add - tells git to include update in the next commit
4. git commit - create a new commit with the current contents and giving a message describing the changes
5. git push - upload files from local to remote repo
6. git pull - download files from remote to local repo
7. git clone - target a repo and make a copy of that repo

#### Assignment Screenshots:

*Screenshot of Java Hello World:

![a1_Java Hello World](img/java_running.png "screenshot of Java Hello World Program")

*Screenshot of AMPSS Running:

![AMPSS Running](img/AMPPS_running.png)

*Screenshot of Andriod Studio First App:

![Andriod Studio First App](img/app_running.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/yanheng_chen/bitbucketstationlocations/ "Bitbucket Station Locations")

